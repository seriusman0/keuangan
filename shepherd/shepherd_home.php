<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Gembala">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body class=" ">
    <div class="main-container">
        <section class="bg--secondary space--sm">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="boxed boxed--lg boxed--border">
                            <div class="text-block text-center">
                                <img alt="avatar" src="img/avatar-round-3.png" class="image--sm" />
                                <span class="h5"><?= $_SESSION['nama'] ?></span>
                                <span>Gembala</span>
                                <span class="label">Gembala</span>
                            </div>
                            <hr>
                            <div class="text-block">
                                <ul class="menu-vertical">
                                    <li>
                                        <a href="#" data-toggle-class=".account-tab:not(.hidden);hidden|#domba;hidden">Domba</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle-class=".account-tab:not(.hidden);hidden|#pengajuan;hidden">Pengajuan</a>
                                    </li>
                                    <li>
                                        <a href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="view" class="boxed boxed--lg boxed--border">
                            <div id="domba" class="account-tab">
                                <h4>Domba</h4>
                                <table class="border--round">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Angkatan</th>
                                            <th>Universitas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $domba = mysqli_query($conn, "SELECT mahasiswa.nama_mhs, mahasiswa.angkatan, kampus.nama_kampus 
                                                            FROM mahasiswa, kampus, gembala 
                                                            WHERE mahasiswa.kampus=kampus.npsn 
                                                            AND mahasiswa.gembala_mhs = gembala.nig 
                                                            and gembala.nama_gembala = '$_SESSION[nama]'");
                                        $no = 1;
                                        while ($i = mysqli_fetch_array($domba)) {
                                            echo "<tr>
                                                    <td>$no</td>
                                                    <td>$i[nama_mhs]</td>
                                                    <td>$i[angkatan]</td>
                                                    <td>$i[nama_kampus]</td>
                                                </tr>";
                                            $no++;
                                        }
                                        ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div id="pengajuan" class="hidden account-tab">
                                <h4>Data Pengajuan Domba</h4>
                                <p>Data pengajuan domba:</p>
                                <table class="border--round">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Subjek</th>
                                            <th>Waktu</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $submission = mysqli_query($conn, "SELECT mahasiswa.nama_mhs, pengajuan.subject, pengajuan.tgl_sub, pengajuan.id_pengajuan FROM pengajuan, mahasiswa where pengajuan.nif = mahasiswa.nif and mahasiswa.nif = 0420206");
                                        $no = 1;
                                        while ($i = mysqli_fetch_array($submission)) {
                                            echo "<tr>
                                                    <td>$no</td>
                                                    <td>$i[nama_mhs]</td>
                                                    <td>$i[subject]</td>
                                                    <td>$i[tgl_sub]</td>
                                                    <td>
                                                        <div>
                                                            <button type='submit' class='btn'>View</button>
                                                        </div>
                                                        <div>
                                                            <button type='submit' class='btn bg--success'>Accept</button>
                                                        </div>
                                                        <a class='btn' href='#&id=$i[id_pengajuan]'></a>

                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                        ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <footer class="footer-3 text-center-xs space--xs ">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img alt="Image" class="logo" src="img/logo-dark.png" />
                        <ul class="list-inline list--hover">
                            <li class="list-inline-item">
                                <a href="#">
                                    <span class="type--fine-print">Get Started</span>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <span class="type--fine-print">help@stack.io</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 text-right text-center-xs">
                        <ul class="social-list list-inline list--hover">
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="socicon socicon-google icon icon--xs"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="socicon socicon-twitter icon icon--xs"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="socicon socicon-facebook icon icon--xs"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="socicon socicon-instagram icon icon--xs"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-md-6">
                        <p class="type--fine-print">
                            Supercharge your web workflow
                        </p>
                    </div>
                    <div class="col-md-6 text-right text-center-xs">
                        <span class="type--fine-print">&copy;
                            <span class="update-year"></span> FLATS.</span>
                        <a class="type--fine-print" href="#">Privacy Policy</a>
                        <a class="type--fine-print" href="#">Legal</a>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </footer>
    </div>
    <!--<div class="loader"></div>-->
    <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
        <i class="stack-interface stack-up-open-big"></i>
    </a>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/flickity.min.js"></script>
    <script src="js/easypiechart.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/typed.min.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/ytplayer.min.js"></script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/granim.min.js"></script>
    <script src="js/jquery.steps.min.js"></script>
    <script src="js/countdown.min.js"></script>
    <script src="js/twitterfetcher.min.js"></script>
    <script src="js/spectragram.min.js"></script>
    <script src="js/smooth-scroll.min.js"></script>
    <script src="js/scripts.js"></script>
</body>

</html>
<?php


?>
<html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Form Pengajuan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Site Description Here">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body class="container">
    <?php include 'navbar.php'; ?>
    <div class="container boxed boxed--border">
        <img src="img/flats_cop_pinjaman.png" alt="">
        <div class="boxed boxed--border">
            <table class="border--round table--alternate-column">
                <tbody>
                    <form action="" method="POST">
                        <tr>
                            <td>Nama</td>
                            <td width=40% colspan="3">
                                <input type="text" name="name" id="name" value="<?= $_SESSION['name'] ?>" disabled>
                            </td>
                            <td>Kampus</td>
                            <td width=40% colspan="3">
                                <input type="text" name="kampus" id="kampus" value="<?= $_SESSION['kampus'] ?>" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td>FLATS / Semester</td>
                            <td>
                                <input type="text" required name="kampus" id="kampus" value="<?= $_SESSION['angkatan'] ?>" disabled>
                            </td>
                            <td>/</td>
                            <td>
                                <div class="input-select">
                                    <select name="semester" id="semester" required>
                                        <option selected="" value=""></option>
                                        <?php for ($i = 1; $i <= 14; $i++) {
                                            echo "<option value='{$i}'>{$i}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                            <td>Tahun Ajaran</td>
                            <td colspan="3">
                                <!-- <input type="text" min="2015" max="2050" name="ta" id="ta" required oninvalid="this.setCustomValidity('Jangan Lupa isi Tahun Ajaran')" oninput="setCustomValidity('')"> -->
                                <div class="input-select">
                                    <select name="ta" id="ta" required>
                                        <option selected="" value="">Tahun Pengajuan</option>
                                        <?php for ($y = 2021; $y <= 2031; $y++) {
                                            echo "<option value='{$y}'>{$y}</option>";
                                        } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>No HP</td>
                            <td colspan="3">
                                <input type="text" name="nohp" id="nohp" placeholder="cth 0821xxxxxxxx" required>
                            </td>
                            <td>IPS / IPK </td>
                            <td>
                                <input type="number" name="ips" id="ips" disabled max="4">
                            </td>
                            <td>/</td>
                            <td>
                                <input type="number" name="ipk" id="ipk" disabled max="4">
                            </td>
                        </tr>
                </tbody>
            </table>

            <table class="border--round">

                <tbody>
                    <tr>
                        <td>Catatan Mahasiswa : </td>
                        <td>Catatan Gembala : </td>
                        <td>Catatan Biro : </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea name="note_c" id="note_c" rows="5" placeholder="Diisi oleh Mahasiswa"></textarea>
                        </td>
                        <td>
                            <textarea name="note_s" id="note_s" rows="5" placeholder="Diisi oleh Gembala" disabled></textarea>
                        </td>
                        <td>
                            <textarea name="note_b" id="note_b" rows="5" placeholder="Diisi oleh Biro" disabled></textarea>
                        </td>
                    </tr>

                </tbody>
            </table>

            <table>

                <tr>
                    <td>Tanggal Pengajuan:</td>
                    <td>Tanggal Revisi Ke-1</td>
                    <td>Tanggal Revisi Ke-2</td>
                    <td>Tanggal Pencairan</td>
                </tr>
                <tr>
                    <td><input type="date" name="tgl_sub" id="tgl_sub" value="<?= date('Y-m-d') ?>"></td>
                    <td><input type="date" name="rev_1" id="rev_1"></td>
                    <td><input type="date" name="rev_2" id="rev_2"></td>
                    <td><input type="date" name="acc" id="acc"></td>
                </tr>
            </table>


            <!-- TABEL PENGAJUAN -->
            <table class="border--round">
                <thead class="bg-primary">
                    <tr>
                        <th>No</th>
                        <th>Jatuh Tempo</th>
                        <th>Pengajuan Biaya Pokok</th>
                        <th>Besaran(Rp.)</th>
                        <th>Acc Biro(Rp.)</th>
                        <th>Biro</th>
                    </tr>
                </thead>
                <tbody>

                    <!-- //perulangan tabel -->
                    <?php $no = 1;
                    while ($no <= 10) {
                        echo
                        "<tr>
                                <td>{$no}</td>
                                <td><input type='date' name='tgl{$no}' id='tgl{$no}'></td>
                                <td><input type='text' name='item{$no}' id='item{$no}'></td>
                                <td><input type='number' name='vP{$no}' id='vP{$no}' required></td>
                                <td><input type='number' name='vAcc{$no}' id='vAcc{$no}' disabled></td>
                                <td>
                                    <div class='input-checkbox'>
                                            <input id='status{$no}' type='checkbox' value='{$no}' name='status{$no}' disabled />
                                        <label for='status{$no}'></label>
                                    </div>
                                </td>
                            </tr>";
                        $no++;
                    }
                    ?>


                    <tr>
                        <td colspan="3" align="right" class="bg--secondary">TOTAL</td>
                        <td><input type="text" id="totalP" disabled></td>
                        <td><input type="text" id="totalAcc" disabled></td>
                        <td>
                            <div class="input-checkbox">
                                <input id="checkbox20" type="checkbox" name="agree" disabled />
                                <label for="checkbox20"></label>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table>

                <tr align="center">
                    <td>Mahasiswa</td>
                    <td>Gembala</td>
                    <td>Keuangan FLATS</td>
                </tr>
                <tr>
                    <td><input type="password" name="password" id="password" required></td>
                    <td><input type="text" disabled></td>
                    <td><input type="text" disabled></td>
                </tr>
            </table>
            <div class="text-primary">
                <i><b>
                        *Password wajib diisi sebagai pengganti tanda tangan untuk keperluan verifikasi form.
                    </b></i>
            </div>
        </div>
    </div>
    </div>
    <input type="text" name="subject" id="subject" placeholder="Subject Pengajuan" required>
    <input type="submit" class="bg-primary" name="submit" id="submit" value="KIRIM">

    <script src="js/flickity.min.js"></script>
    <script src="js/easypiechart.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/typed.min.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/ytplayer.min.js"></script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/granim.min.js"></script>
    <script src="js/jquery.steps.min.js"></script>
    <script src="js/countdown.min.js"></script>
    <script src="js/twitterfetcher.min.js"></script>
    <script src="js/spectragram.min.js"></script>
    <script src="js/smooth-scroll.min.js"></script>
    <script src="js/scripts.js"></script>

</body>
</form>
<footer>

</footer>

</html>
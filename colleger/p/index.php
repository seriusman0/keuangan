<?php
$nif = $_SESSION['nif'];
$query = mysqli_query($conn, "SELECT * FROM pinjaman WHERE pinjaman.nif='$nif' ");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include "header.php"; ?>
</head>

<body>
    <?php include 'navbar.php' ?>
    <div class="container">
        <div>
            <a class="btn" href="index.php?page=insert_pinjaman">
                <span class="btn__text">
                    Tambah Pinjaman
                </span>
            </a>
        </div>
        <table class="border--round">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tanggal</td>
                    <td>Subject</td>
                    <td>Status</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                while ($row = mysqli_fetch_array($query)) {
                    echo "<tr>
                        <td align='center'>$no</td>
                        <td>" . tgl_indo($row["tgl_sub"]) . "</td>
                        <td>$row[subject]</td>
                        <td>" . sub_status($row["status"]) . "</td>
                        <td align='center'>
                        <a href='index.php?page=viewpinjaman&id=" . $row['id_pinjaman'] . "'class='btn'>View</a>
                        <a href='#' class='btn'>Hapus</a>
                        <a href='index.php?page=view_pinjaman_as_shepherd&id=" . $row['id_pinjaman'] . "'class='btn'>As Shepherd</a>
                        ";

                    $no++;
                } ?>

                <tr>
                </tr>
            </tbody>
        </table>
    </div>

</body>

</html>
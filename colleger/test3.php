<?php

$no = 1;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test perulangan</title>
</head>

<body>
    <table>
        <tr>
            <th>1
            </th>
        </tr>
        <?php while ($no <= 10) {
            echo "<tr>
                    <td>{$no}
                    </td>
                </tr>";
            $no++;
        } ?>

    </table>
</body>

</html>
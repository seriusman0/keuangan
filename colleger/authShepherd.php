<?php
if (isset($_POST['validasi'])) {
    $inputPass = $_POST['passShepherd'];
    $id_pengajuan = $_GET['id'];
    //ambil password gembala dari id pengajuan 
    $passShepherd = mysqli_fetch_array(mysqli_query($conn, "select password_gembala from pengajuan, mahasiswa, gembala where 
        pengajuan.id_pengajuan = '$id_pengajuan' &&
        pengajuan.nif = mahasiswa.nif &&
        mahasiswa.gembala_mhs = gembala.nig"))[0];
    if (password_verify($inputPass, $passShepherd)) {
        $_SESSION['authShepherd'] = "allow";
        header("Location:index.php?page=view_as_shepherdTrue&id=$id_pengajuan");
    } else {
        echo "<script>alert('Password Salah')</script>";
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Autentikasi Gembala</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Site Description Here">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="bar__module">
        <a class="btn btn--sm type--uppercase" href="index.php">
            <span class="btn__text">
                HOME
            </span>
        </a>
        <a class="btn btn--sm btn--primary type--uppercase" href="logout.php">
            <span class="btn__text">
                Logout
            </span>
        </a>
    </div>
    <a id="start"></a>

    <div class="main-container">
        <section class="text-center height-50">
            <div class="container pos-vertical-center">
                <div class="row">
                    <div class="col-md-8 col-lg-6">
                        <h1>Cek Validasi Gembala</h1>
                        <p class="lead">
                            Untuk menampilkan pengajuan sebagai gembala, silakan masukkan password gembala.
                        </p>
                        <form action="" method="POST">
                            <div class="">
                                <div class="">
                                    <input type="password" autocomplete="off" placeholder="Password Gembala" name="passShepherd" />
                                </div>
                                <div>
                                    <button class="btn btn--primary type--uppercase" type="submit" name="validasi">Validasi</button>
                                </div>
                            </div>
                            <!--end of row-->
                        </form>
                    </div>
                </div>
                <div class="container">

                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

    </div>
    <!--<div class="loader"></div>-->
    <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
        <i class="stack-interface stack-up-open-big"></i>
    </a>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/flickity.min.js"></script>
    <script src="js/easypiechart.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/typed.min.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/ytplayer.min.js"></script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/granim.min.js"></script>
    <script src="js/jquery.steps.min.js"></script>
    <script src="js/countdown.min.js"></script>
    <script src="js/twitterfetcher.min.js"></script>
    <script src="js/spectragram.min.js"></script>
    <script src="js/smooth-scroll.min.js"></script>
    <script src="js/scripts.js"></script>
</body>

</html>